import FreeCAD as App
import FreeCADGui as Gui
import os
from FreeCAD import (Base, MeshPart, Part)

from hmc_utils.misc import get_logger
from webcolors import name_to_rgb

unit_vector_x = App.Vector(1, 0, 0)
unit_vector_y = App.Vector(0, 1, 0)
unit_vector_z = App.Vector(0, 0, 1)
origin = App.Vector(0, 0, 0)


class MeshGroup(object):
    """
    An class to hold a group of faces which are meshed together to form one boundary
    """

    def __init__(self, doc, gad, name="Default", display_mode="Wireframe", lighting="Two side",
                 resolution=1000.0, mefisto_tesselation=False, surface_deviation=1.0,
                 fineness=None, second_order=0, optimize=1, allow_quad=0,
                 growth_rate=0.1, seg_per_edge=6, seg_per_radius=10
                 ):
        self.logger = get_logger(__name__)
        self.name = name
        self.doc = doc
        self.gad = gad
        self.resolution = resolution
        self.meshobj = doc.addObject("Mesh::Feature", name)
        self.meshobj.Label = name
        self.meshgui = gad.getObject(name)
        self.meshgui.DisplayMode = display_mode
        self.meshgui.Lighting = lighting

        self.fineness = fineness
        self.growth_rate = growth_rate
        self.seg_per_edge = seg_per_edge
        self.seg_per_radius = seg_per_radius
        self.second_order = second_order
        self.optimize = optimize
        self.allow_quad = allow_quad

        self.mefisto_tesselation = mefisto_tesselation
        self.surface_deviation = surface_deviation

        # some properties to keep track of
        self.faces = list()
        self.area = 0
        self.mesh_triangles = list()
        self.shape = None
        self.mesh = None

    def add_face_to_mesh(self, face):
        """
        adding the a face to the mesh

        Parameters
        ----------
        face: object
            A single face object
        """

        # MeshPart.meshFromShape(Shape=__doc__.getObject("Cut030").Shape, Fineness=2, SecondOrder=0,
        #                       Optimize=1, AllowQuad=0)

        self.area += face.Area
        if self.mefisto_tesselation:
            self.logger.info("Making triangle with mefisto tesselation")
            # MaxLength=self.resolution,
            triangles = MeshPart.meshFromShape(Shape=face,
                                               Fineness=self.fineness,
                                               SecondOrder=self.second_order,
                                               Optimize=self.optimize,
                                               AllowQuad=0).Topology
        else:
            self.logger.info("Making triangle with normal tesselation")
            triangles = face.tessellate(self.surface_deviation)

        for tri in triangles[1]:
            f = []
            for k in range(3):
                vindex = tri[k]
                f.append(triangles[0][vindex])
                self.mesh_triangles.append(f)

    def faces_to_mesh(self):

        self.logger.info("adding {} faces to the shape".format(len(self.faces)))
        # self.shape = Part.Shape(self.faces)
        # fuse = self.doc.addObject("Part::MultiFuse", "Fuse faces of " + self.name)
        # fuse.Shapes = self.faces

        self.shape = Part.makeShell(self.faces)
        self.doc.recompute()

        if self.fineness is None:
            self.logger.info("Creating mesh from shape with user defined")
            self.meshobj.Mesh = MeshPart.meshFromShape(Shape=self.shape,
                                                       GrowthRate=self.growth_rate,
                                                       SegPerEdge=self.seg_per_edge,
                                                       SegPerRadius=self.seg_per_radius,
                                                       SecondOrder=self.second_order,
                                                       Optimize=self.optimize,
                                                       AllowQuad=0)
        else:
            self.logger.info("Creating mesh from shape with user finess setting {}: "
                             "optimize {}".format(self.fineness, self.optimize))
            self.meshobj.Mesh = MeshPart.meshFromShape(Shape=self.shape,
                                                       Fineness=self.fineness,
                                                       SecondOrder=self.second_order,
                                                       Optimize=self.optimize,
                                                       AllowQuad=0
                                                       )
        self.doc.recompute()


def recompute_and_centralize_view(self):
    """
    Recompute geometry and fit view to part.
    """
    self.doc.recompute()
    Gui.activeView().viewTop()
    Gui.SendMsgToActiveView("ViewFit")
    Gui.exec_loop()


class FCGeometryBase(object):
    """
    Base class to create a free cad geometry and mesh

    Parameters
    ----------
    file_name: str, optional
        Name of the FCstd file if you want to start from an existing FreeCAD file. Default = None,
        which will initialise a new document
    document_name: str, optional
        Name of the document. Default = None . If a file name is passed, the file name is given
    active_work_bench: str, optional
        Name of the workbech to import at the start

    """

    def __init__(self, file_name=None, document_name="Default",
                 active_work_bench="CompleteWorkbench"):
        self.logger = get_logger(__name__)
        self.logger.debug("Open window in {}".format(document_name))

        self.file_name = file_name
        self.file_name_base = None

        self.components = dict()

        # reference to the final geometry
        self.geometry = None
        self.shape = None
        self.surface_mesh = None
        self.faces = None
        self.n_faces = None

        self.mesh_collections = dict()

        Gui.showMainWindow()
        Gui.activateWorkbench(active_work_bench)

        App.newDocument(document_name)

        if file_name is None:
            # initialise a new document
            self.doc = App.activeDocument()
            self.gad = Gui.activeDocument()
        else:
            self.file_name_base = os.path.split(os.path.splitext(file_name)[0])[1]
            App.open(file_name)
            App.setActiveDocument(self.file_name_base)
            self.doc = App.getDocument(self.file_name_base)
            self.gad = Gui.getDocument(self.file_name_base)

    def store_component(self, component):
        """
        Store the component in the dictionary under the key corresponding to the component name

        Parameters
        ----------
        component: object
            Component to store
        """

        label = component.Label
        self.components[label] = component

    def make_block(self, name="block",
                   length=1,
                   width=1,
                   height=1,
                   centralize_x=False,
                   centralize_y=False,
                   centralize_z=False,
                   centralize_all=False,
                   position=Base.Vector(0, 0, 0)
                   ):
        """
        Make a block

        Parameters
        ----------
        name: str, optional
            Name of th block. Default = "block"
        length: float, optional
            Length of the block (size in x -direction). Default = 1
        width: float, optional
            Length of the block (size in x -direction). Default = 1
        height: float, optional
            Length of the block (size in x -direction). Default = 1
        centralize_x: bool
            If true, but the block in x-direction to the center
        centralize_y
            If true, but the block in y-direction to the center
        centralize_z
            If true, but the block in z-direction to the center
        centralize_all
            If true, but the block in all directions to the center
        position: Base.Vector, optional
            Position of the block

        Returns
        -------
        object
            Block object

        """

        block = self.doc.addObject("Part::Box", name)
        block.Length = length
        block.Width = width  # Width is in y direction
        block.Height = height
        move_to = position
        if centralize_x or centralize_all:
            move_to -= Base.Vector(length / 2, 0, 0)
        if centralize_y or centralize_all:
            move_to -= Base.Vector(0, width / 2, 0)
        if centralize_z or centralize_all:
            move_to -= Base.Vector(0, 0, height / 2)

        block.Placement.move(move_to)

        return block

    def make_cylinder(self, name="cylinder",
                      radius=1,
                      height=1,
                      direction="Z",
                      center_of_rotation=origin,
                      centralize=False,
                      position=Base.Vector(0, 0, 0)
                      ):
        """
        Make a cylinder

        Parameters
        ----------
        name: str,  optional.
            Name of the cylinder. Default = cylinder
        radius: float, optional
            Radius of the cylinder. Default = 1
        height: float
            Height of the cylinder. Default = 1
        direction: str
            Direction of the cylinder
        center_of_rotation: Base.Vector, optional
            Center to rotate about. Default is 0, 0, 0
        centralize: bool
            If true, but the centre of the cylinder at the position
        position: Base.Vector
            Position of the cylinder

        Returns
        -------
        object:
            Cylinder object

        """

        cyl = self.doc.addObject("Part::Cylinder", name)
        cyl.Height = height
        cyl.Radius = radius  # Width is in y direction
        move_to = position

        base = cyl.Placement.Base

        if direction[0] == "-":
            sign = -1
        else:
            sign = 1

        if direction in ("X", "-X"):
            rotation = App.Rotation(unit_vector_y, sign * 90)
            if centralize:
                move_to -= Base.Vector(sign * height / 2, 0, 0)
        elif direction in ("Y", "-Y"):
            rotation = App.Rotation(unit_vector_x, -sign * 90)
            if centralize:
                move_to -= Base.Vector(0, sign * height / 2, 0)
        elif direction in ("Z", "-Z"):
            rotation = App.Rotation(unit_vector_z, 0)
            if centralize:
                move_to -= Base.Vector(0, 0, sign * height / 2)
        else:
            raise AssertionError("direction should be either 'X', 'Y', or 'Z'. Found {} ".format(
                direction))

        cyl.Placement = App.Placement(base, rotation, center_of_rotation)

        cyl.Placement.move(move_to)

        return cyl

    def make_sphere(self,
                    name="sphere",
                    radius=1,
                    position=Base.Vector(0, 0, 0)
                    ):
        """
        Make a sphere

        Parameters
        ----------
        name: str
            Name of the component
        radius: float, optional
            Radius of the sphere. Default = 1
        position: Base.Vector, optionalo
            Location of the sphere. Default = Base.Vector(0, 0, 0)

        Returns
        -------
        object:
            Sphere object

        """

        sphere = self.doc.addObject("Part::Sphere", name)
        sphere.Radius = radius  # Width is in y direction
        move_to = position

        sphere.Placement.move(move_to)

        return sphere

    def fuse_objects(self, name="Fused", objects=None, set_source_object_invisible=True):
        """
        Fuse multiple objects into one

        Parameters
        ----------
        name: str, optional
            New object name.  Default = "Fused"
        objects: list
            List of object to fuse
        set_source_object_invisible: bool
            If true, make all the source objects invisible

        Returns
        -------
        Object
            Fused object

        """

        fuse = self.doc.addObject("Part::MultiFuse", name)

        if objects is not None:
            fuse.Shapes = objects

        if set_source_object_invisible:
            # set all the source objects invisible
            for obj in objects:
                label = obj.Label
                objgui = self.gad.getObject(label)
                objgui.Visibility = False

        self.doc.recompute()

        return fuse

    def set_object_properties(self,
                              component,
                              color="white",
                              transparency=0,
                              display_mode="Flat Lines",
                              visible=True):
        """
        Set the visual properties of the object 'component'
        Parameters
        ----------
        component: str or object
            Component reference or name to set the visual properties of the component
        color: str
            Name of the color to set the properties
        transparency: float
            How transparent the  object should be
        display_mode: {"Flat Lines", "Shadow", "Wireframe", "Points"}
            How to display the object
        visible: bool
            If false, hide the object

        """
        if not isinstance(component, str):
            # we passed the component name (not hte object itself). Get the name from the Label
            label = component.Label
        else:
            label = component

        # get the gui object beloning to label and set the visual properties
        object_gui = self.gad.getObject(label)
        object_gui.Visibility = visible
        object_gui.Transparency = transparency
        if color is not None:
            # convert the tuple with RGB code (0~255) to float in range (0~1)
            object_gui.ShapeColor = tuple([c / 255 for c in name_to_rgb(color)])
        object_gui.DisplayMode = display_mode

    def save_mesh_to_file(self, file_name, title="Default"):
        """
        Save the current surface mesh to file

        Parameters
        ----------
        file_name: str
            File name to save to
        title: str
            Titie to use
        """

        self.surface_mesh.Mesh.write(file_name, "AST", title)

    def make_mesh_object(self, object_name=None, max_global_length=1000.0,
                         mefisto_tesselation_global=False, surface_deviation=10.,
                         repair_mesh=False, mesh_mode="one_part", mesh_groups=None,
                         write_to_file=False, default_boundary_name="wall_boundary",
                         growth_rate_global=0.1,
                         seg_per_radius_global=6, seg_per_edge_global=10,
                         fineness_global=None):
        """
        Make a mesh object

        Parameters
        ----------
        name: str, optional
            Name of the mesh object to create
        max_global_length: float
            Maximum edge length

        Returns
        -------
        object
            Mesh object
        """

        if object_name is not None:
            self.geometry = self.doc.getObjectsByLabel(object_name)[0]
        else:
            self.geometry = self.doc.ActiveObject

        self.shape = self.geometry.Shape

        self.faces = self.shape.Faces
        self.n_faces = len(self.faces)

        if mesh_mode == "one_part":
            mesh_group = MeshGroup(doc=self.doc, gad=self.gad,
                                   name="Whole", surface_deviation=surface_deviation,
                                   resolution=max_global_length)
            self.mesh_collections["one_part"] = mesh_group
        elif mesh_mode == "all":
            for ii, face in enumerate(self.faces):
                face_name = "face_{:03d}".format(ii)
                mesh_group = MeshGroup(doc=self.doc, gad=self.gad,
                                       name=face_name, resolution=max_global_length,
                                       surface_deviation=surface_deviation,
                                       mefisto_tesselation=mefisto_tesselation_global,
                                       growth_rate=growth_rate_global,
                                       seg_per_edge=seg_per_edge_global,
                                       fineness=fineness_global,
                                       seg_per_radius=seg_per_radius_global
                                       )
                mesh_group.faces.append(face)
                self.mesh_collections[face_name] = mesh_group
        elif mesh_mode == "parts":
            added_faces = list()
            # no loop over the specified group names with the list of face id's and add them
            for group_name, group_prop in mesh_groups.items():
                try:
                    resolution = group_prop["resolution"]
                except KeyError:
                    resolution = max_global_length
                try:
                    growth_rate = group_prop["growth_rate"]
                except KeyError:
                    growth_rate = growth_rate_global
                try:
                    seg_per_edge = group_prop["seg_per_edge"]
                except KeyError:
                    seg_per_edge = seg_per_edge_global
                try:
                    seg_per_radius = group_prop["seg_per_radius"]
                except KeyError:
                    seg_per_radius = seg_per_radius_global
                try:
                    fineness = group_prop["fineness"]
                except KeyError:
                    fineness = fineness_global
                try:
                    mefisto_tesselation = group_prop["mefisto_tesselation"]
                except KeyError:
                    mefisto_tesselation = mefisto_tesselation_global

                self.mesh_collections[group_name] = MeshGroup(doc=self.doc, gad=self.gad,
                                                              name=group_name,
                                                              resolution=resolution,
                                                              growth_rate=growth_rate,
                                                              seg_per_edge=seg_per_edge,
                                                              fineness=fineness,
                                                              seg_per_radius=seg_per_radius,
                                                              surface_deviation=surface_deviation,
                                                              mefisto_tesselation=
                                                              mefisto_tesselation)

                face_indices = list()

                # select the faces based on a explicit list
                try:
                    f_indices = group_prop["faces"]
                except KeyError:
                    self.logger.debug("No faces defined in group {}".format(group_name))
                else:
                    if not isinstance(f_indices, list):
                        f_indices = [f_indices]
                    face_indices.extend(f_indices)

                # select the faces based on a bounding box
                try:
                    bounding_box = group_prop["bounding_box"]
                except KeyError:
                    self.logger.debug("No bounding box defined in group {}".format(group_name))
                else:
                    c_min = bounding_box["min_corner"]
                    c_max = bounding_box["max_corner"]
                    self.logger.info("Find faces with box {} {}".format(c_min, c_max))
                    f_indices = list()
                    # loop over all faces
                    for face_index, face in enumerate(self.faces):
                        # now check for this faces if all the vertices are within the bounding box
                        # if one vertex is outside, stop and do not add this face
                        face_is_in_box = True
                        for vertex in face.Vertexes:
                            for comp in ["X", "Y", "Z"]:
                                vertex_comp = getattr(vertex, comp)
                                if vertex_comp < c_min[comp] or vertex_comp > c_max[comp]:
                                    face_is_in_box = False
                                    break
                            if not face_is_in_box:
                                break
                        if face_is_in_box:
                            # this face is within the bounding box. Now check if it is not already
                            # added in one of the other groups explicit lists
                            face_in_other_group = False
                            for group_name_2, group_prop_2 in mesh_groups.items():
                                try:
                                    f_indices_2 = group_prop_2["faces"]
                                except KeyError:
                                    pass
                                else:
                                    if not isinstance(f_indices_2, list):
                                        f_indices_2 = [f_indices_2]
                                    if face_index in f_indices_2:
                                        face_in_other_group = True

                            if not face_in_other_group:
                                # in box is still true, so the whole triangle is with the
                                self.logger.info("Adding face {} to BB group {}".format(face_index,
                                                                                        group_name))
                                f_indices.append(face_index)

                    # add all the faces which were within the bounding box to the new faces to add
                    face_indices.extend(f_indices)

                for ii in face_indices:
                    face = self.faces[ii]
                    self.mesh_collections[group_name].faces.append(face)
                    added_faces.append(ii)

            # now create a default group part which contains all non-defined faces
            self.mesh_collections[default_boundary_name] = MeshGroup(doc=self.doc, gad=self.gad,
                                                                     name=default_boundary_name,
                                                                     resolution=max_global_length,
                                                                     growth_rate=growth_rate,
                                                                     seg_per_edge=seg_per_edge,
                                                                     seg_per_radius=seg_per_radius,
                                                                     surface_deviation=
                                                                     surface_deviation,
                                                                     mefisto_tesselation=
                                                                     mefisto_tesselation)
            for ii, face in enumerate(self.faces):
                if ii in added_faces:
                    # we have already added this face to one of the defined groups above. Skip
                    continue
                else:
                    # add this face to the default group
                    self.mesh_collections[default_boundary_name].faces.append(face)

        else:
            raise AssertionError("mode must be either 'one_part' or 'all'. found {}".format(
                mesh_mode))

        areatotal = 0
        for group_name, mesh_group in self.mesh_collections.items():
            self.logger.info("Adding group {}".format(group_name))

            if mesh_mode == "one_part":
                mesh_group.meshobj.Mesh = MeshPart.meshFromShape(Shape=self.shape,
                                                                 MaxLength=max_global_length)
            else:

                # for face in mesh_group.faces:
                # mesh_group.add_face_to_mesh(face)

                # if len(mesh_group.mesh_triangles) > 0:
                #    # assign the faces to the meshobj
                #    mesh_group.meshobj.Mesh = Mesh.Mesh(mesh_group.mesh_triangles)

                mesh_group.faces_to_mesh()

            areatotal += mesh_group.area
            if repair_mesh:
                mesh_group.meshobj.harmonizeNormals()
                mesh_group.meshobj.harmonizeNormals()
                mesh_group.meshobj.harmonizeNormals()
                mesh_group.meshobj.removeFoldsOnSurface()
                mesh_group.meshobj.harmonizeNormals()
                mesh_group.meshobj.removeNonManifolds()
                mesh_group.meshobj.removeDuplicatedFacets()
                mesh_group.meshobj.removeFoldsOnSurface()
                # mesh_group.meshobj.fixSelfIntersections()

            if write_to_file:

                file_base = os.path.splitext(self.file_name)[0]
                out_file = "_".join([file_base, group_name]) + ".stl"
                self.logger.info("Writing mesh to {}".format(out_file))
                try:
                    mesh_group.meshobj.Mesh.write(out_file, "AST", group_name)
                except Base.FreeCADError as err:
                    self.logger.warning(err)

        self.logger.info("Mesh total area of {}".format(areatotal))

    def recompute_and_centralize_view(self):
        """
        Recompute geometry and fit view to part.
        """
        self.doc.recompute()
        Gui.activeView().viewTop()
        Gui.SendMsgToActiveView("ViewFit")
        Gui.exec_loop()
