import argparse
import logging
import sys

from hmc_utils.misc import (create_logger, get_logger, PackageInfo, get_clean_version,
                            read_settings_file)

import freecad_geometries
from freecad_geometries.fc_geometry_base import (FCGeometryBase)

PACKAGE_INFO = PackageInfo(freecad_geometries)
__version__ = PACKAGE_INFO.package_version


class MeshGeometry(FCGeometryBase):
    def __init__(self, file_name):
        self.logger = get_logger(__name__)
        super(MeshGeometry, self).__init__(file_name=file_name)

        self.logger.info("Start processing {}".format(file_name))

    def create_surface_mesh(self, mesh_name, object_name, max_global_length=1,
                            mefisto_tesselation=False, mesh_mode="all", mesh_groups=None,
                            write_stl_to_file=False, default_boundary_name="wall_boundary",
                            fineness=None, growth_rate=0.1, seg_per_radius=6, seg_per_edge=10):
        """
        Create the surface mesh
        """
        self.logger.info("Start creating the surface mesh")

        self.make_mesh_object(object_name=object_name, max_global_length=max_global_length,
                              mesh_mode=mesh_mode, mesh_groups=mesh_groups,
                              mefisto_tesselation_global=mefisto_tesselation,
                              write_to_file=write_stl_to_file,
                              default_boundary_name=default_boundary_name,
                              fineness_global=fineness,
                              growth_rate_global=growth_rate, seg_per_radius_global=seg_per_radius,
                              seg_per_edge_global=seg_per_edge)


def _parse_command_line_arguments():
    """ Parser for utility """
    parser = argparse.ArgumentParser(description="A script for manipulate the fatigue database",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    # set the verbosity level command line arguments
    parser.add_argument("-d", "--debug", help="Print lots of debugging statements",
                        action="store_const", dest="log_level", const=logging.DEBUG,
                        default=logging.INFO)
    parser.add_argument("-v", "--verbose", help="Be verbose", action="store_const",
                        dest="log_level",
                        const=logging.INFO)
    parser.add_argument("-q", "--quiet", help="Be quiet: no output", action="store_const",
                        dest="log_level",
                        const=logging.WARNING)

    parser.add_argument("--keep_open", help="Keep the gui open for viewing the result",
                        default=True, action="store_true")
    parser.add_argument("--mefisto_tesselation", help="Use mefisto tesselation ",
                        default=None, action="store_true")
    parser.add_argument("--no_mefisto_tesselation", help="Do not mefisto tesselation ",
                        default=None, action="store_false", dest="mefisto_tesselation")
    parser.add_argument("--fineness", help="Degree of mesh fineness. 0=course up to 4 is very fine")
    parser.add_argument("--growth_rate", help="Growth rate of the STL file. Smaller is finer mesh",
                        default=0.1)
    parser.add_argument("--seg_per_edge", help="Number of segments per edge", default=6)
    parser.add_argument("--seg_per_radius", help="Number of segments per radius", default=10)
    parser.add_argument("--close", dest="keep_open", action="store_false",
                        help="Close the gui after processing")
    parser.add_argument("--mesher", help="Create the surface mesh", action="store_true",
                        default=None)
    parser.add_argument("--no-mesher", help="Do not create the surface mesh",
                        dest="mesher", action="store_false")
    parser.add_argument("--maximum_mesh_global_length", default=None, type=float,
                        help="The maximum edge length of the surface mesh")
    parser.add_argument("--out_mesh_file_name", default="mesh.stl", type=str,
                        help="Output name of the mesh file")
    parser.add_argument("--file_name", help="Input file name of type FCstd")
    parser.add_argument("--object_name", help="Name of the object to mesh")
    parser.add_argument("--mesh_name", help="Name of the mesh ", default="Balast Tank")
    parser.add_argument("--mesh_mode", help="Type of mesh stl made",
                        default=None, choices=("all", "part", "one_part"))
    parser.add_argument("--write_stl_to_file", help="write the meshed surfaces to file",
                        default=None, action="store_true")
    parser.add_argument("settings_file", help="File name with the process settings")

    # note that the following option are not parsed via the parser but directly extracted from
    # the sys.argv list. You have to write them out on the command line completely
    if set(sys.argv).intersection(["--version", "--version_long", "--full_revision_id"]):

        # if --version or versionlong is passed on the command line, show the version and exit
        if "--version" in sys.argv:
            # full version is not requested, so clean it
            version = get_clean_version(__version__)
        else:
            version = __version__
        print("version : {}".format(version))
        if "--full_revision_id" in sys.argv:
            print("git SHA        : {}".format(PACKAGE_INFO.git_sha))
            print("Python version : {}".format(PACKAGE_INFO.python_version))
            print("Build date     : {}".format(PACKAGE_INFO.build_date))
        sys.exit(0)

    # parse the command line
    args = parser.parse_args()

    return args, parser


def run():
    args, parser = _parse_command_line_arguments()

    create_logger(console_log_level=args.log_level,
                  file_log_format_long=False, console_log_format_clean=True)

    settings = read_settings_file(args.settings_file)

    # if the freecad file name is given, take that as leading. Otherwise take the one from the
    # settings file
    if args.file_name is not None:
        file_name = args.file_name
    else:
        file_name = settings["general"]["file_name"]

    if args.object_name is not None:
        object_name = args.object_name
    else:
        object_name = settings["general"]["object_name"]

    if args.maximum_mesh_global_length is not None:
        maximum_mesh_global_length = args.maximum_mesh_global_length
    else:
        maximum_mesh_global_length = settings["general"]["maximum_mesh_global_length"]

    if args.mefisto_tesselation is not None:
        mefisto_tesselation = args.mefisto_tesselation
    else:
        mefisto_tesselation = settings["general"]["mefisto_tesselation"]

    if args.fineness is not None:
        fineness = args.fineness
    else:
        fineness = settings["general"]["fineness"]

    if args.mesh_mode is not None:
        mesh_mode = args.mesh_mode
    else:
        mesh_mode = settings["general"]["mesh_mode"]

    if args.mesh_name is not None:
        mesh_name = args.mesh_name
    else:
        mesh_name = settings["general"]["mesh_name"]

    if args.mesher is not None:
        create_mesh = args.mesher
    else:
        create_mesh = settings["general"]["create_mesh"]

    if args.write_stl_to_file is not None:
        write_stl_to_file = args.write_stl_to_file
    else:
        write_stl_to_file = settings["general"]["write_stl_to_file"]

    if args.growth_rate is not None:
        growth_rate = args.growth_rate
    else:
        growth_rate = settings["general"]["growth_rate"]

    if args.seg_per_edge is not None:
        seg_per_edge = args.seg_per_edge
    else:
        seg_per_edge = settings["general"]["seg_per_edge"]

    if args.seg_per_radius is not None:
        seg_per_radius = args.seg_per_radius
    else:
        seg_per_radius = settings["general"]["seg_per_radius"]

    default_boundary_name = settings["general"]["default_boundary_name"]

    mesh_groups = settings["mesh_groups"]

    geo = MeshGeometry(file_name=file_name)

    if create_mesh:
        geo.create_surface_mesh(
            mesh_name=mesh_name,
            object_name=object_name,
            mesh_groups=mesh_groups,
            max_global_length=maximum_mesh_global_length,
            mefisto_tesselation=mefisto_tesselation,
            fineness=fineness,
            growth_rate=growth_rate,
            seg_per_radius=seg_per_radius,
            seg_per_edge=seg_per_edge,
            mesh_mode=mesh_mode,
            write_stl_to_file=write_stl_to_file,
            default_boundary_name=default_boundary_name
        )

    # geo.save_mesh_to_file(file_name=args.out_mesh_file_name)

    if args.keep_open:
        geo.recompute_and_centralize_view()


if __name__ == "__main__":
    run()
