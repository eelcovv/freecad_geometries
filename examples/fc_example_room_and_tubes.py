import argparse
import logging
import sys
from FreeCAD import Base

from hmc_utils.misc import (create_logger, get_logger, PackageInfo, get_clean_version)

import freecad_geometries
from freecad_geometries.fc_geometry_base import (FCGeometryBase)

PACKAGE_INFO = PackageInfo(freecad_geometries)
__version__ = PACKAGE_INFO.package_version


class RoomAndTubes(FCGeometryBase):
    def __init__(self, document_name="RoomAndTubes", active_work_bench="CompleteWorkbench"):
        self.logger = get_logger(__name__)
        super(RoomAndTubes, self).__init__(document_name=document_name,
                                           active_work_bench=active_work_bench)

        self.logger.info("Start processing {}".format(document_name))

    def create_geometry(self):
        """
        Create a room with some object, fuse them and export as STL
        """
        room = self.make_block(name="room",
                               length=10,
                               width=5,
                               height=20,
                               centralize_x=True,
                               position=Base.Vector(2, 0, 1)
                               )
        self.store_component(room)
        self.set_object_properties(room, color="brown")

        tube1 = self.make_cylinder(name="tube1",
                                   height=10,
                                   radius=4,
                                   direction="Z",
                                   centralize=False,
                                   position=Base.Vector(0, 0, 10)
                                   )
        self.store_component(tube1)
        self.set_object_properties("tube1", color="crimson")

        sphere_1 = self.make_sphere(name="sphere1",
                                    radius=3,
                                    position=Base.Vector(0, 0, 0)
                                    )
        self.store_component(sphere_1)
        self.set_object_properties(sphere_1, color="navy")

        merged = self.fuse_objects("SomeBasicObjects", objects=[room, tube1, sphere_1])
        self.set_object_properties(merged, display_mode="Wireframe")

        self.geometry = merged

        for name, comp in self.components.items():
            self.logger.info("Have component: {}".format(name))

    def create_surface_mesh(self, max_global_length=1):
        """
        Create the surface mesh
        """
        self.logger.info("Start creating the surface mesh")

        self.make_mesh_object(max_global_length=max_global_length)


def _parse_command_line_arguments():
    """ Parser for utility """
    parser = argparse.ArgumentParser(description="A script for manipulate the fatigue database",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    # set the verbosity level command line arguments
    parser.add_argument("-d", "--debug", help="Print lots of debugging statements",
                        action="store_const", dest="log_level", const=logging.DEBUG,
                        default=logging.INFO)
    parser.add_argument("-v", "--verbose", help="Be verbose", action="store_const",
                        dest="log_level",
                        const=logging.INFO)
    parser.add_argument("-q", "--quiet", help="Be quiet: no output", action="store_const",
                        dest="log_level",
                        const=logging.WARNING)

    parser.add_argument("--keep_open", help="Keep the gui open for viewing the result",
                        default=True, action="store_true")
    parser.add_argument("--close", dest="keep_open", action="store_false",
                        help="Close the gui after processing")
    parser.add_argument("--mesher", help="Create the surface mesh", action="store_true")
    parser.add_argument("--no-mesher", help="Do not create the surface mesh",
                        dest="mesher", action="store_false")
    parser.add_argument("--maximum_mesh_global_length", default=1.0, type=float,
                        help="The maximum edge length of the surface mesh"
                        )
    parser.add_argument("--out_mesh_file_name", default="mesh.stl", type=str,
                        help="Output name of the mesh file")

    # note that the following option are not parsed via the parser but directly extracted from
    # the sys.argv list. You have to write them out on the command line completely
    if set(sys.argv).intersection(["--version", "--version_long", "--full_revision_id"]):

        # if --version or versionlong is passed on the command line, show the version and exit
        if "--version" in sys.argv:
            # full version is not requested, so clean it
            version = get_clean_version(__version__)
        else:
            version = __version__
        print("version : {}".format(version))
        if "--full_revision_id" in sys.argv:
            print("git SHA        : {}".format(PACKAGE_INFO.git_sha))
            print("Python version : {}".format(PACKAGE_INFO.python_version))
            print("Build date     : {}".format(PACKAGE_INFO.build_date))
        sys.exit(0)

    # parse the command line
    args = parser.parse_args()

    return args, parser


def run():
    args, parser = _parse_command_line_arguments()

    create_logger(console_log_level=args.log_level,
                  file_log_format_long=False, console_log_format_clean=True)

    geo = RoomAndTubes(document_name="FirstRoom")

    geo.create_geometry()

    if args.mesher:
        geo.create_surface_mesh(max_global_length=args.maximum_mesh_global_length)

        #geo.save_mesh_to_file(file_name=args.out_mesh_file_name)

    if args.keep_open:
        geo.recompute_and_centralize_view()


if __name__ == "__main__":
    run()
